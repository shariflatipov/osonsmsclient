﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Net.Http;

namespace OsonSMSClient
{
    class SMSApi
    {
        public async void send(string msisdn, string text)
        {
            HttpClient client = new HttpClient();
            var dlm = ";";
            var password = Properties.Settings.Default.pass;
            var login = Properties.Settings.Default.login;
            var sender = Properties.Settings.Default.sender;
            var txn_id = sha256_hash(UnixTimestamp());
            Console.WriteLine(UnixTimestamp().ToString());

            var str_hash = sha256_hash(txn_id + dlm + login + dlm + sender + dlm + msisdn + dlm + password);

            var url = string.Format(
                "http://82.196.1.18/sendsms_v1.php?phone_number={0}&from={1}&sender={2}&t={3}&msg={4}&login={5}&txn_id={6}&str_hash={7}",
                msisdn, login, sender, 23, text, login, txn_id, str_hash);

            HttpResponseMessage response = await client.GetAsync(url);
            HttpContent content = response.Content;
            string result = await content.ReadAsStringAsync();
            Console.WriteLine(result);
        }

        public static String sha256_hash(String value)
        {
            using (SHA256 hash = SHA256Managed.Create())
            {
                return String.Concat(hash
                  .ComputeHash(Encoding.UTF8.GetBytes(value))
                  .Select(item => item.ToString("x2")));
            }
        }

        public static string UnixTimestamp()
        {
            return System.Guid.NewGuid().ToString();
        }
    }
}
