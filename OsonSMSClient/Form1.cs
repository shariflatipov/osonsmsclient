﻿using System;
using System.Windows.Forms;


namespace OsonSMSClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Registry u = new Registry();
            string data = u.Read();
            var phoneText = data.Split(Properties.Settings.Default.generalSplitter);

            foreach (var dt in phoneText)
            {
                var d = dt.Split(Properties.Settings.Default.splitter);
                dataGridView1.Rows.Add(d[0], d[1]);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SMSApi api = new SMSApi();
            foreach(DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells["phone"].Value != null && !String.IsNullOrWhiteSpace(row.Cells["phone"].Value.ToString()))
                {
                    api.send(row.Cells["phone"].Value.ToString(), row.Cells["text"].Value.ToString());
                }
            }
        }
    }
}
